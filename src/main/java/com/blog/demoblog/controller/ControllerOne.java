package com.blog.demoblog.controller;

import com.blog.demoblog.Entity.CommentEntity;
import com.blog.demoblog.Entity.PostEntity;
import com.blog.demoblog.Entity.Post_Tag;
import com.blog.demoblog.Entity.TagEntity;
import com.blog.demoblog.repository.TagRepository;
import com.blog.demoblog.service.CommentService;
import com.blog.demoblog.service.PostService;
import com.blog.demoblog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Controller
public class ControllerOne {

    @Autowired
    PostService ps;
    @Autowired
    CommentService cs;
    @Autowired
    TagService ts;
    @Autowired
    TagRepository tagrepo;

    @GetMapping("/")
    public String viewBlogs(Model model) {
        List<PostEntity> list = ps.getAllPost();
        model.addAttribute("blogs", list);
        return "viewBlog";
    }

    @RequestMapping(value = "/addBlog", method = RequestMethod.GET)
    public String addBlog(@ModelAttribute PostEntity postEntity ,@RequestParam("tags") String tags) {
        ps.addPost(postEntity);


        //Saving Tag Part
        List<TagEntity> listOfAll = tagrepo.findAll();
        String[] tagArray = tags.split(",");
        List<String> tagList = new ArrayList<>(Arrays.asList(tagArray));
        List<String> PosttagList = new ArrayList<>(Arrays.asList(tagArray));

        for(TagEntity tagEntity: listOfAll) {
            tagList.remove(tagEntity.getName());
        }
        for(String str : tagList){
            TagEntity tagEntity = new TagEntity();
            tagEntity.setName(str);
            ts.saveTag(tagEntity);
        }

        for(String name : PosttagList){
            Post_Tag postTag = new Post_Tag();
            postTag.setPost_id(postEntity.getId());
            postTag.setTag_id();
        }
        return "redirect:/";
    }
    @RequestMapping(value = "/editAddBlog/{blogId}")
    public String editaddBlog(@ModelAttribute PostEntity postEntity,@PathVariable Long blogId) {
        postEntity.setId(blogId);
        ps.addPost(postEntity);
        return "redirect:/";
    }


    @RequestMapping("/newBlog")
    public String newBlog() {
        return "newBlog";
    }



    @RequestMapping("/blogLink/{blogId}")
    public String update(@PathVariable Long blogId, Model model , Model m2) {
        Integer intBlogId = blogId.intValue();
        PostEntity postEntity = ps.getPostById(intBlogId);
        model.addAttribute("blog", postEntity);

        List<CommentEntity> list1 = cs.getAllComment();
        List<CommentEntity> list2 = new ArrayList<>();

        for(CommentEntity ce : list1) {
            if(Objects.equals(ce.getPost_id(), blogId)){
                list2.add(ce);
            }
        }
        m2.addAttribute("commentsList",list2);
        return "update-page";
    }

   @RequestMapping("/editBlog/{blogId}")
    public String edit(@PathVariable Long blogId, Model model){
       Integer intBlogId = blogId.intValue();
       PostEntity postEntity = ps.getPostById(intBlogId);
       model.addAttribute("blog",postEntity);
       return "editBlog";
   }
   @RequestMapping("/delete/{blogId}")
   public String delete(@PathVariable Long blogId, Model model){
       Integer intBlogId = blogId.intValue();
       ps.deleteBlogById(intBlogId);
       return "redirect:/";
   }
}
