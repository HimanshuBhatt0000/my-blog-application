package com.blog.demoblog.service;

import com.blog.demoblog.Entity.TagEntity;
import com.blog.demoblog.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TagService {
    @Autowired
    TagRepository tagRepository;
public void saveTag(TagEntity tagEntity){
    tagRepository.save(tagEntity);
}
}
