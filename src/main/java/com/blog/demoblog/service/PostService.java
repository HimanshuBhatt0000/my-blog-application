package com.blog.demoblog.service;

import com.blog.demoblog.Entity.PostEntity;
import com.blog.demoblog.repository.PostRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {

    @Autowired
    private PostRepository postRepository;

    public void addPost(PostEntity post){
        postRepository.save(post);
    }

    public List<PostEntity> getAllPost(){
        return postRepository.findAll();
    }

    public PostEntity getPostById(Integer id){
        return postRepository.getReferenceById(id);
    }

    public void deleteBlogById(Integer blogId) {
        // Use the deleteById method from the PostRepository to delete the blog by its ID

        postRepository.deleteById(blogId);
    }
}
