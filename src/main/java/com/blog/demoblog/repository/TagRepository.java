package com.blog.demoblog.repository;

import com.blog.demoblog.Entity.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<TagEntity,Integer> {
}
