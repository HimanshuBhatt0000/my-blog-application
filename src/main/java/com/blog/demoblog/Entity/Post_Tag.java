package com.blog.demoblog.Entity;

import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
public class Post_Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int pk ;
    private Long post_id;

    private Long tag_id;

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public Long getPost_id() {
        return post_id;
    }

    public void setPost_id(Long post_id) {
        this.post_id = post_id;
    }

    public Long getTag_id() {
        return tag_id;
    }

    public void setTag_id(Long tag_id) {
        this.tag_id = tag_id;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    private Date created_at;

    private Date updated_at;



}
